package com.mxd.spider;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletContextInitializer;

@SpringBootApplication
public class SpiderApplication implements ServletContextInitializer{
	
	public static void main(String[] args) throws IOException {
		
		SpringApplication.run(SpiderApplication.class, args);
	}

	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		servletContext.setInitParameter("org.apache.tomcat.websocket.textBufferSize", "" + (1024 * 1024));
	}
}
