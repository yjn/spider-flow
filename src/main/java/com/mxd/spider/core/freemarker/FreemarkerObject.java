package com.mxd.spider.core.freemarker;

public class FreemarkerObject {
	
	private Object value;

	public FreemarkerObject(Object value) {
		super();
		this.value = value;
	}

	public Object getValue() {
		return value;
	}
}
